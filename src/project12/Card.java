/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project12;

import java.util.*;

/**
 *
 * @author s208606111
 */
public class Card 
{
    
    private int mRank; //initialize the rank (1,2,3...plus , PlusTwo...)
    private int mColor; //initialize the suit (Red, Blue...)
    
    //constructor
    public Card(int Color, int rank)
    {
        this.mRank = rank;
        this.mColor = Color;
    }//end construcor
    
    //getter method
    public int getRank()
    {
        return mRank; 
    }//end get rank
    
    //setter method
    public void setRank(int rank)
    {
        this.mRank = rank;
    }//end set rank

    public int getColor() {
        return mColor;
    }

    public void setColor(int mColor) {
        this.mColor = mColor;
    }
    
    
    
    @Override    /// need to run over other stuff that was in toString (not for sure)
    public String toString()
    {
        
    //combine rank and Color together into a single string(example: Plus of Red)

        ////suing StringBuilder for modifiability later on
        
        String displayCard = new String();   // string for the texts
        
        switch(mRank){
            //since rank is int type, now we need to match int 10 to String Plus...17 to king
            case 1:
                displayCard = "One";
                break;
            case 3:
                displayCard = "Three";
                break;
            case 4:
                displayCard = "Four";
                break;    
            case 5:
                displayCard = "Five";
                break;   
            case 6:
                displayCard = "Six";
                break;   
            case 7:
                displayCard = "Seven";
                break;
            case 8:
                displayCard = "Eight";
                break; 
            case 9:
                displayCard = "Nine";
                break; 
            case 10:
                displayCard = "Plus";
                break;
            case 11:
                displayCard = "PlusTwo";
                break;
            case 12:
                displayCard = "Stop";
                break;
            case 13:
                displayCard = "ChangeDiraction";
                break;    
            case 14:
                displayCard = "Taki";
                break;   
            case 15:
                displayCard = "ChangeColor";
                break;   
            case 16:
                displayCard = "SuperTaki";
                break;  
            case 17:
                displayCard = "King";
                break; 
            default:
                //displayCard = String.valueOf(rank); //number from 2 to 10 does not need to modify
                break;
        }//end rank switch
  
        displayCard += " of "; //setting the format of the output
        
        switch(mColor){
            case 0:
                displayCard += "Red";
                break;
            case 1:
                displayCard += "Blue";
                break;
            case 2:
                displayCard += "Green";
                break;
            case 3:
                displayCard += "Yellow";
                break;
            case 4:
                displayCard += "No Color";
                break;
            default: //anything else, do nothing
                break;
        }//end suit switch
        
        //return the result of an entire cmombined string
        return displayCard;
    }//end toString
    
}//end Card Class
    
    
    
    
    
    
    
    
    
    /*
    public ECard mCardType; // 
    public EColor mCardColor;
    public boolean mIsSpecial;
    
    public Card() ///the defult. if user dosent enter data
    {
        mCardType = ECard.One;
        mCardColor = EColor.Blue;
        mIsSpecial = false;
    }
    public Card(ECard type, EColor color) //Function Builder
    {
        mCardType = type; // gets from the enum (ECard) the card type 
        mCardColor = color; // gets from the enum (EColor) the card Color 
        mIsSpecial = IsSpecialCard(type); // gets from the func IsSpecielCard if card is Special or regular
    }
    
    
    
    public boolean IsSpecialCard(ECard type) // cheks if the card is Special or regular
    {
        switch (type) 
        {
            case Stop : return true;  // if it is equle stop then send to public Card(ECard type, EColor color) that it is a Special card
            case ChangeDiraction: return true;
            case Plus: return true;
        }
        return false;  // defult, (if all above is not good). sends back to public Card (ECard type, EColor color) that the card is a regular card
    }
    
            
    */
        

    
