/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project12;

/**
 *
 * @author s208606111
 */
public enum ECard  // enum for the card value
{
    One(1),Three(3),Four (4),Five(5) ,Six (6) ,Seven(7) ,Eight(8) , Nine(9), Ten(10),  // the regular cards
    Plus(11), PlusTwo(12), Stop(13), ChangeDiraction(14), ChangeColor(15) ,SuperTaki(16) ,Taki(17) ,King(18) ;// the Special cards
    
    
    private int numVal;  // need to lern all below

    ECard(int numVal) {
        this.numVal = numVal;
    }
    public int getNumVal() {
        return numVal;
    }
}

